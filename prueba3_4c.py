def copiar_m(matriz:list)->list:
    result=[]
    for i in matriz:
        result.append(i[:])
    return result

def crear_m(filas:int,columnas:int)->list:
    matriz=[]
    contador=1

    for i in range(filas):
        matriz.append([])
        for j in range(columnas):
            matriz[i].append(contador)
            contador+=1
    return matriz

def rotar_elemento(matriz:list,p:int, fila, columna)->list:
    if p==0:
        ubicar=[fila,columna]
        return ubicar
    copia=copiar_m(matriz)
    if ((fila==0) & (columna==n)) | ((fila==m) & (j!=0)):
        if fila==0:
            mover=n-columna
            if mover>=p:
                indc=columna+p
                copia[fila][indc]=matriz[fila][columna]
                p=0
                rotar_elemento(copia,p,0,indc)
            else:
                p=p-mover
                rotar_elemento(copia,p,0,n)
        else:
            mover=columna
            if mover>=p:
                indc=columna-p
                copia[fila][indc]=matriz[fila][columna]
                p=0
                rotar_elemento(copia,p,m,indc)
            else:
                p=p-mover
                rotar_elemento(copia,p,m,0)
    elif ((columna==0) & (fila!=0)) | ((columna==n) & (fila!=m)):
        if columna==n:
            mover=m-fila
            if mover>=p:
                indf=fila+p
                copia[indf][columna]=matriz[fila][columna]
                p=0
                rotar_elemento(copia,p,indf,n)
            else:
                p=p-mover
                rotar_elemento(copia,p,m,n)
        else:
            mover=fila
            if mover>=p:
                indf=fila-p
                copia[indf][columna]=matriz[fila][columna]
                p=0
                rotar_elemento(copia,p,indf,0)
            else:
                p=p-mover
                rotar_elemento(copia,p,0,0)
    else:
        ubicar=[fila,columna]
        return ubicar
    

columnas_n=4
filas_m=4

n,m=columnas_n-1, filas_m-1

matriz=crear_m(filas_m,columnas_n)
print(matriz)
lista=copiar_m(matriz)
p=2

for i in range(filas_m):
    for j in range(columnas_n):
        ubicacion=rotar_elemento(matriz,p,i,j)
        lista[ubicacion[0]][ubicacion[1]]=matriz[i][j]

print(lista)



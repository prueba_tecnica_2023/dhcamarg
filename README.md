# dhcamarg

# Repositorio para pruebas tecnicas

Este repositorio es parte de un proceso de selección llevado a cabo para la vacante del Centro de Excelencia.

## Requisitos

En este repositorio, se almacenan códigos SQL desarrollados para mySQL 8.0.33, así como se utiliza un ambiente virtual basado en Python 3.9.2 con sus librerias respectivas. Adicionalmente, el desarrollo de los códigos se lleva a cabo en una maquina virtual con sistema operativo Ubuntu 20.04.

para tener archivo al repositorio, se puede clonar haciendo uso de:

`git clone https://gitlab.com/prueba_tecnica_2023/dhcamarg.git`

O si se tienen llaves SSH configuradas, se puede clonar usando:

`git clone git@gitlab.com:prueba_tecnica_2023/dhcamarg.git`

También está la posibilidad de contar con un repositorio local ya creado, en este caso se puede usar:

```
cd path_al_repositorio
git remote add origin https://gitlab.com/prueba_tecnica_2023/dhcamarg.git
git branch -M main
git push -uf origin main
```

# Archivos de la prueba

En esta sección se encuntra una descripción de los archivos generados como respuestas a los puntos enviados el documento .pdf de la prueba técnica

## SQL

Los archivos .sql son:

* crear_subir.sql: Este archivo se generó para crear el esquema de la base de datos, así como para subir la información almacenada en formato csv. Para el cargue de la información, mediante el script de jupyter notebook *exploratorio.ipynb*, en el cual, además de realizar un análisis exploratorio inicial, se limpia la información para poder tener un archivo csv en un formato ideal para realizar la subida de la información sin problemas. Adicionalmente, este proceso se llevo por medio de la terminal, en el caso de Ubuntu, el archivo csv debe estar en **/var/lib/mysql-files/** para subir la información sin problemas, por lo que una vez descargados los archivos csv se deben ejecutar los comandos `sudo mv obligaciones.csv /var/lib/mysql-files/obligaciones_clientes.csv` y `sudo mv tasas.csv /var/lib/mysql-files/tasas.csv`, dentro de la carpeta donde se encuentran estos. Sin embargo, se reconoce que existen software como workbench, que permiten hacer este proceso por medio de ventanas y menús de una manera más amigable.

* script3_1.sql: En este script se crean las tablas y procesos necesarios para dar respuesta al punto 3.1 de la prueba técnica. En este punto, se considera que se deben priorizar clientes cuya valor final total sea *igual o mayor a 42 millones*, puesto que con estos clientes (**25% del total**), tenemos el **80% de los ingresos futuros**.

* script3_2.sql: En este script, se generan los query's que de manera teórica se deberian ejecutar para llegar a los resultados deseados.

## Python

Para desarrollar los puntos en python, se recurre a utilizar un archivo de **jupyter notebook** para dar solución al punto 3.3 y archivos individuales python para los literales del punto 3.4.

Se recomienda la creación del ambiente virtual y su posterior activación. Tras esto último, se instalan las librerias que se encuentran en el archivo de *requirements.txt*. Para esto si el sistema es Windows:

```
python -m virtualenv venv
path\donde\esta\el\ambiente\venv\Scripts\activate
cd paht\al\repositorio\
pip install -r requirements.txt
```

Y si se posee un sistema basado en Linux, como Ubuntu:

```
python3 -m virtualenv venv
source path/donde/esta/el/ambiente/venv/bin/activate
cd paht/al/repositorio/
pip install -r requirements.txt
```

En el caso del archivo 3.3 se encuentra todos los procesos para subir los archivos de excel por medio de la libreria de pandas,  generar el reporte de un cliente que se consultara por medio de su número de documento y un modelo de regresión multiple. Al final del archivo se encuentra el análisis de este último, junto con las limitaciones del estudio.

Para el punto 3.4, se crearon scripts individuales por cada literal con el fin de separar cada tarea solicitada de manera modular. Estos scripts son:

* prueba3_4a.py: Programa que lee una secuencia de números insertada por teclado, calcula el promedio general, toma los valores de la lista mayores a ese promedio y calcula nuevamente el promedio sobre dichos valores. Se le indica al usuario que debe insertar números y que el programa se cierra cuando inserta el -1.

* prueba3_4b.py: Programa que recibe una secuencia de números para crear una lista, con esta y un número que se le solicita al usuario, se calcula los dos elementos de la lista cuya suma sea más cercana al número insertado, junto con la suma de estos elementos.

* prueba3_4c.py: Intento fallido para rotar los elementos de una matriz.

## Licencia

Este repositorio se creo con información pública como parte del proceso de selección del centro de excelencia.

## Estatus del repositorio

El último cambio generado es del 01/Ago/2023
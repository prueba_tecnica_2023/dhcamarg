def filtrar_mayor(x:int)->bool:
    '''
    DOC: Función creada para comprobar si un número X de una lista es mayor al promedio de la lista
    general
    '''
    if x>promedio:
        return True
    else:
        return False

entradas=[]
cantidad=0
suma=0
numero=int(input("""
Favor insertar un número.

El programa termina al insertar el -1
"""))

while numero!=-1:
    entradas.append(numero)
    cantidad+=1
    suma=suma+numero
    numero=int(input("""
Favor insertar un número.

El programa termina al insertar el -1
"""))

try:
    promedio=suma/cantidad
    lista_filtrada=list(filter(filtrar_mayor,entradas))
    suma_filtrada=0
    cantidad_filtrada=0
    for i in lista_filtrada:
        suma_filtrada=i+suma_filtrada
        cantidad_filtrada+=1
    promedio_filtrado=suma_filtrada/cantidad_filtrada
    print(f"""El promedio para los números por encima de {promedio},
          es de {promedio_filtrado}""")
except:
    print("su número insertado es -1")



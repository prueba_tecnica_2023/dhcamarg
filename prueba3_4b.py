def suma_cercana(lista:list, numero:int)->str:
    '''
    DOC: Función que recibe una lista, la ordena y después arroja los dos elementos cuya suma es más 
    cercana a un número insertado como argumento de esta función. El resultado se imprime por pantalla.
    '''
    lista.sort()
    v=10000
    for i in range(0,len(lista)):
        for j in range(i+1,len(lista)):
            suma=lista[i]+lista[j]
            diff=abs(suma-numero)
            if v<diff:
                next
            else:
                v=diff
                mejor=[i,j]

    print(f"""los números cuya suma es más cercana es 
        {lista[mejor[0]]} y {lista[mejor[1]]} y su suma es de
        {lista[mejor[0]]+lista[mejor[1]]}""")




lista=[]
try:
    elemento=int(input("""
Ingresar un número para añadir a la lista, cuando desee terminar
dejar en blanco
            """))
    lista.append(elemento)
except:
    print("Ha dejado una lista vacia")
    exit()


while True:
    try:
        elemento=int(input("""
Ingresar un número para añadir a la lista, cuando desee terminar
dejar en blanco
            """))
        lista.append(elemento)
    except:
        print("Ha terminado la lista")
        break

x=int(input("""Ingresar un número
            """))

suma_cercana(lista=lista,numero=x)

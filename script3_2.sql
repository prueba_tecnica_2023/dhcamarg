
-- Solución literal a
select Cedula,
        Nombre
from CLIENTE
;

-- Solución literal b
select Cedula,
        Nombre
from CLIENTE
where Region="Centro"
;

-- Solución literal c
select a.Cedula_cliente,
        count(a.Estado) as conteo
from CUENTAS as a
LEFT JOIN CLIENTE as b
ON a.Cedula_cliente=b.Cedula
WHERE a.Estado="Activa"
GROUP BY a.Cedula_cliente
having count(a.Estado)>3

-- Solución literal d
select a.Nombre
from CLIENTE as a
right join CLAVE_DINAMICA as b
ON a.Cedula=b.cedual_cliente
WHERE a.Nombre IS NOT NULL

-- Solución literal e
select a.Nombre,
        b.cedula_cliente
from CLIENTE as a
left join CLAVE_DINAMICA as b
ON a.Cedula=b.cedual_cliente
WHERE b.cedula_cliente IS NULL

-- Solición literal f
select b.Region,
        sum(a.Saldo) as saldo_total
from CUENTAS as a
left join CLIENTE as b
ON a.Cedula_cliente=b.Cedula
group by b.Region

-- Solución literal g
select a.Estado,
        sum(a.Saldo) as saldo_total
from CUENTAS as a
left join CLIENTE as b
ON a.Cedula_cliente=b.Cedula
where a.Estado="Activa" and
a.Fecha_apertura between 20180501 and 20180531
group by a.Estado
create database if not exists prueba_tecnica;

use prueba_tecnica;

CREATE TABLE if not exists obligaciones_clientes (
    radicado bigint NOT NULL,
    num_documento bigint NOT NULL,
    cod_segm_tasa varchar(255),
    cod_subsegm_tasa int NULL,
    cal_interna_tasa varchar(255) NULL,
    id_producto varchar(255) NOT NULL,
    tipo_id_producto varchar(255) NULL,
    valor_inicial float NULL,
    fecha_desembolso datetime NULL,
    plazo float NULL,
    cod_periodicidad int NULL,
    periodicidad varchar(255) NULL,
    saldo_deuda float NULL,
    modalidad varchar(255) NULL,
    tipo_plazo varchar(255) NULL
    )
    CHARACTER SET latin1;

CREATE TABLE if not exists tasas (
    cod_segmento varchar(5) NOT NULL,
    segmento varchar(50) NOT NULL,
    cod_subsegmento varchar(5),
    calificacion_riesgos varchar(10),
    tasa_cartera float NOT NULL,
    tasa_operacion_especifica float NOT NULL,
    tasa_hipotecario float NOT NULL,
    tasa_leasing float NOT NULL,
    tasa_sufi float NOT NULL,
    tasa_factoring float NOT NULL,
    tasa_tarjeta float NOT NULL
);

LOAD DATA INFILE
"/var/lib/mysql-files/obligaciones_clientes.csv"
INTO TABLE obligaciones_clientes
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;

LOAD DATA INFILE
"/var/lib/mysql-files/tasas.csv"
INTO TABLE tasas
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;
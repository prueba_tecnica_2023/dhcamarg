USE prueba_tecnica;

/* 
Query para solución de los númerales 1, 2 y 3 del punto 3.1 del 
proceso de selección del Centro de Excelencia.
*/
create table if not exists financiero as
with cte as (
select a.num_documento,
		a.plazo,
        a.cod_periodicidad as periodicidad,
        a.valor_inicial,
        trim(substring_index(a.id_producto,"-",-1)) as producto,
        b.tasa_cartera as tc,
        b.tasa_operacion_especifica as toe,
        b.tasa_hipotecario as thipo,
        b.tasa_leasing as tl,
        b.tasa_sufi as tsufi,
        b.tasa_factoring as tf,
        b.tasa_tarjeta as ttdc
from obligaciones_clientes as a
left join tasas as b
on concat(a.cod_segm_tasa,a.cod_subsegm_tasa,a.cal_interna_tasa)=concat(b.cod_segmento,b.cod_subsegmento,b.calificacion_riesgos)
), cte2 as(
select num_documento,
		producto,
        valor_inicial,
        periodicidad,
		case
			when producto="operacion_especifica" then toe
            when producto IN ("Cartera Total","cartera") then tc
            when producto IN ("leasing","Leasing Cartera Total") then tl
            when producto IN ("tarjeta","Tarjeta de Crédito") then ttdc
            when producto="Sufi" then tsufi
            when producto="factoring" then tf
            when producto="Hipotecario" then thipo
		end as tasa
from cte
), cte3 as (
select num_documento,
		producto,
        valor_inicial,
        periodicidad,
        tasa,
        round((pow(1+tasa,periodicidad/12)-1),6) as tasa_efectiva
from cte2
)
select num_documento,
		producto,
        valor_inicial,
        periodicidad,
        tasa,
        tasa_efectiva,
        tasa_efectiva*valor_inicial as valor_final
from cte3;

select *
from financiero

/*
Solución del númeral 4 del punto 3.1, con aproximadamente el 25% 
de los clientes tenemos el 80% de los ingresos por lo cual se usa
esté limite, lo cual presenta que se mantienen los clientes cuyo 
valor final acumulado sea mayor a 42 millones
*/

with cte as(
select num_documento,
		sum(valor_final) as valor_cliente
from financiero
group by num_documento
order by valor_cliente desc
), cte2 as (
select num_documento,
		valor_cliente,
		sum(valor_cliente) over(order by valor_cliente desc)/ sum(valor_cliente) over() as total
from cte
)
select num_documento,
		valor_cliente
from cte2
where total<0.81
;